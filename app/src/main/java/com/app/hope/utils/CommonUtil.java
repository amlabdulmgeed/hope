package com.app.hope.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.app.hope.App.AppController;
import com.app.hope.BuildConfig;
import com.app.hope.R;
import com.app.hope.UI.views.Toaster;

import java.io.IOException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.PortUnreachableException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static android.text.format.DateUtils.MINUTE_IN_MILLIS;


public class CommonUtil {

    public static boolean isALog = true;

    public static void onPrintLog(Object o) {
        if (isALog) {
            Log.e("Response >>>>", new Gson().toJson(o));
        }
    }

    public static void PrintLogE(String print) {
        if (BuildConfig.DEBUG) {
            Log.e(AppController.TAG, print);
        }
        Log.e(AppController.TAG, print);
    }

    public static String makeURL(double sourceLat, double sourceLog, double destLat, double destLog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourceLat));
        urlString.append(",");
        urlString.append(Double.toString(sourceLog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destLat));
        urlString.append(",");
        urlString.append(Double.toString(destLog));
        urlString.append("&sensor=false&mode=DRIVING&alternatives=true");
        urlString.append(",");
        urlString.append("&key=AIzaSyArHOxwlpYCDQRkmyRxuO_L4rMr4KMPxbs");


        return urlString.toString();
    }

    public static String getLanguage() {
        String language = Locale.getDefault().getDisplayLanguage();
        return language;


    }

    public static void requestFocus(View view, Window window) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static void ShareApp(Context context) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out mktbty app at: https://play.google.com/store/apps/details?id=com.example.zboon");
        sendIntent.setType("text/plain");
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(sendIntent);
    }

    public static void RateApp(AppCompatActivity context) {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static int handleException(Context context, Throwable t) {
        if (t instanceof SocketTimeoutException) {
            makeToast(context, R.string.time_out_error);
            return R.string.time_out_error;
        } else if (t instanceof UnknownHostException) {
            makeToast(context, R.string.connection_error);
            return R.string.connection_error;
        } else if (t instanceof ConnectException) {
            makeToast(context, R.string.connection_error);
            return R.string.connection_error;
        } else if (t instanceof NoRouteToHostException) {
            makeToast(context, R.string.connection_error);
            return R.string.connection_error;
        } else if (t instanceof PortUnreachableException) {
            makeToast(context, R.string.connection_error);
            return R.string.connection_error;
        } else if (t instanceof UnknownServiceException) {
            makeToast(context, R.string.connection_error);
            return R.string.connection_error;
        } else if (t instanceof BindException) {
            makeToast(context, R.string.connection_error);
            return R.string.connection_error;
        } else {
            makeToast(context, R.string.connection_error);
            return R.string.connection_error;
        }
    }

    public static void makeToast(Context context, int msgId) {
        Toaster toaster = new Toaster(context);
        toaster.makeToast(context.getString(msgId));

    }

    public static void makeToast(Context context, String msg) {
        Toaster toaster = new Toaster(context);
        toaster.makeToast(msg);

    }

    public static void setConfig(String language, Context context) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

    }

    public static Bitmap onGetBitmap(String photoPath, Bitmap bitmap) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static String getFormattedTime(String date) {
        Date parse = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            parse = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parse);
        long updated = calendar.getTimeInMillis();
        return DateUtils.getRelativeTimeSpanString(updated, System.currentTimeMillis(), MINUTE_IN_MILLIS).toString();
    }

    public void showParElevation(boolean showHide, AppBarLayout app_bar, float elevation) {
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            if (showHide) {
                app_bar.setElevation(elevation);

            } else {
                app_bar.setElevation((float) 0.0);
            }
        }
    }


    // Glide url
    public static DrawableRequestBuilder<String> loadImage(Context context, String posterPath) {
        return Glide
                .with(context)
                .load(posterPath)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    public static void openWhatsappContact(AppCompatActivity context, String number) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent mWhatsAppIntent = new Intent(Intent.ACTION_SENDTO, uri);
        mWhatsAppIntent.setPackage("com.whatsapp");
        context.startActivity(Intent.createChooser(mWhatsAppIntent, ""));
    }


    public static int getStakenumbers(Context context) {
        ActivityManager m = (ActivityManager) context
                .getSystemService(context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> runningTaskInfoList = m.getRunningTasks(10);
        Iterator<RunningTaskInfo> itr = runningTaskInfoList.iterator();
        int numOfActivities = 0;
        while (itr.hasNext()) {
            RunningTaskInfo runningTaskInfo = (RunningTaskInfo) itr.next();
            int id = runningTaskInfo.id;
            CharSequence desc = runningTaskInfo.description;
            numOfActivities = runningTaskInfo.numActivities;
            String topActivity = runningTaskInfo.topActivity
                    .getShortClassName();
            CommonUtil.PrintLogE("Activities number : " + numOfActivities + " Top Activies : " + topActivity);
            return numOfActivities;
        }
        return numOfActivities;
    }


    public static void setStrokInText(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
    public static void setStrokBelowText(TextView textView) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

}

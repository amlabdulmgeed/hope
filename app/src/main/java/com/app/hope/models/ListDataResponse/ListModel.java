package com.app.hope.models.ListDataResponse;

import java.io.Serializable;



public class ListModel implements Serializable {

    public ListModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * id : 1
     * name : Afghanistan
     */



    private int id;

    private String name;

    private String imageUrl;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}

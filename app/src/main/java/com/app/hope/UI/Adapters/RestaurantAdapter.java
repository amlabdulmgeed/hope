package com.app.hope.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.TextView;

import com.app.hope.R;
import com.app.hope.UI.Activities.RestDetailsActivity;
import com.app.hope.models.RestaurantModel;

import com.bumptech.glide.Glide;

import java.util.ArrayList;



public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.ViewHolder> {

    ArrayList<RestaurantModel> arrayList;
    LayoutInflater layoutInflater;
    ItemClickListener clickListener;
    Context context;


    public RestaurantAdapter(Context context, ArrayList<RestaurantModel> arrayList) {
        this.arrayList = arrayList;
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;


    }

    public RestaurantAdapter(ArrayList<RestaurantModel> exampleList) {
        arrayList = exampleList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = layoutInflater.inflate(R.layout.item_menue, viewGroup, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.textView.setText(arrayList.get(i).getRest_name());
        viewHolder.textView1.setText(arrayList.get(i).getRest_type());
        Glide.with(context).load(arrayList.get(i).getRest_img()).asBitmap().placeholder(R.drawable.ic_image_add_button).into(viewHolder.imageView);
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestDetailsActivity.startActivity((AppCompatActivity) context, arrayList.get(i).getRest_id(),arrayList.get(i).getRest_name());
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView textView, textView1;
        CardView cardView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cardView=(CardView)itemView.findViewById(R.id.cv_item);
            textView1 = (TextView) itemView.findViewById(R.id.tv_ctg_count);
            imageView = (ImageView) itemView.findViewById(R.id.iv_item);

            textView = (TextView) itemView.findViewById(R.id.tv_ctg_name);



        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public interface ItemClickListener {
        void onClick(View view, int possition);


    }

    public void filterList(ArrayList<RestaurantModel> filteredList) {
        arrayList = filteredList;
        notifyDataSetChanged();
    }

}
package com.app.hope.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RestaurantResponse extends BaseResponse {
    @SerializedName("return")
    private ArrayList<RestaurantModel> x;

    public ArrayList<RestaurantModel> getX() {
        return x;
    }

    public void setX(ArrayList<RestaurantModel> x) {
        this.x = x;
    }
}

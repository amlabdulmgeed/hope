package com.app.hope.models;

import com.google.gson.annotations.SerializedName;
public class UserResponse extends BaseResponse {
    @SerializedName("return")
    private UserModel x;

    public UserModel getX() {
        return x;
    }

    public void setX(UserModel x) {
        this.x = x;
    }
}

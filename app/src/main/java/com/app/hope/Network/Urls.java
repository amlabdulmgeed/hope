package com.app.hope.Network;

public class Urls {

    public final static String GETLOCATION = "geocode/json?sensor=true";

    public final static String ENDPOINT = "https://www.otlob.accuragroup-eg.net/api/";
    public final static String ENDPOINT2="https://www.talabat.art4muslim.net/api/";
    public final static String LOGIN="login";
    public final static String GET_RESTAURANTS="getResturants";
    public final static String GET_RESTAURANTS_DETAILS="getOrder";
}

package com.app.hope.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.app.hope.Network.RetroWeb;
import com.app.hope.Network.ServiceApi;
import com.app.hope.R;
import com.app.hope.base.ParentActivity;
import com.app.hope.models.UserResponse;
import com.app.hope.utils.CommonUtil;
import com.google.gson.JsonObject;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends ParentActivity {

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_password)
    EditText etPassword;


    public  void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, LogInActivity.class);
        mAppCompatActivity.startActivity(mIntent);
        overridePendingTransition(R.anim.slide_right, R.anim.slide_out_left);
    }

    @Override
    protected void initializeComponents() {
        if (mSharedPrefManager.getLoginStatus()){
            HomeActivity.startActivity(mActivity);
            finish();
        }
    }

    private void login(String mobile, String password) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("mobile", mobile);
        jsonObject.addProperty("password", password);
        jsonObject.addProperty("access_key", "Gdka52DASWE3DSasWE742Wq");
        jsonObject.addProperty("access_password", "yH52dDDF85sddEWqPNV7D12sW5e");
        RetroWeb.getClient().create(ServiceApi.class).login("application/json", jsonObject).
                enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                        mSharedPrefManager.setGuest(false);
                        mSharedPrefManager.setUserData(response.body().getX());
                        mSharedPrefManager.setLoginStatus(true);
                        HomeActivity.startActivity(mActivity);
                        finish();

                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                    }


                });
    }

    public void signIn(View view) {

        login(etPhone.getText().toString(),
                etPassword.getText().toString());


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_log_in;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}

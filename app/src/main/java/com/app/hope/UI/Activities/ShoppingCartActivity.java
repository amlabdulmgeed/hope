package com.app.hope.UI.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.app.hope.R;
import com.app.hope.UI.Adapters.CartDetailsAdapter;

import com.app.hope.base.ParentActivity;
import com.app.hope.models.OrderDetailsModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;


public class ShoppingCartActivity extends ParentActivity {

    @BindView(R.id.rv_recycle)
    RecyclerView recyclerView;
    CartDetailsAdapter adapter;
    ArrayList<OrderDetailsModel> list;
    @BindView(R.id.tv_total)
    TextView tvTotal;
    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;
    @BindView(R.id.iv_arrow)
    ImageView ivArrow;
    @BindView(R.id.coordinatorLayout)

    CoordinatorLayout coordinatorLayout;
    BottomSheetBehavior sheetBehavior;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, ShoppingCartActivity.class);
        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {
        ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.downarrow));

        try {
            if (mSharedPrefManager.getMyCart().size() == 0) {
                tvTotal.setText("0");
                Toast.makeText(getApplicationContext(), "No items in cart!", Toast.LENGTH_SHORT).show();
            } else {
                tvTotal.setText(mSharedPrefManager.getMyCart().size() + "");
                buildRecyclerView();
                adapter = new CartDetailsAdapter(mContext, mSharedPrefManager.getMyCart());
                recyclerView.setAdapter(adapter);
                runLayoutAnimation(recyclerView, R.anim.layout_animation_slide_right);
                recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mContext, R.anim.layout_animation_slide_right));
            }
        } catch (Exception e) {
            tvTotal.setText("0");
            Toast.makeText(getApplicationContext(), "No items in cart!", Toast.LENGTH_SHORT).show();
        }


        sheetBehavior = BottomSheetBehavior.from(bottomSheet);


        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.uparrow));
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        ivArrow.setImageDrawable(getResources().getDrawable(R.drawable.downarrow));
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


}

    @OnClick(R.id.tv_open)
    public void openbottom() {
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);


    }

    @OnClick(R.id.btn_checkout)
    public void toggleBottomSheet() {
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


    }


    private void buildRecyclerView() {


        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new CartDetailsAdapter(mContext, list);
        recyclerView.setHasFixedSize(true);


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
}

package com.app.hope.Network;


import com.app.hope.models.RestDetailsResponse;
import com.app.hope.models.RestaurantResponse;
import com.app.hope.models.UserResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServiceApi {

    @POST(Urls.LOGIN)
    Call<UserResponse> login(
            @Header("Content-Type") String contentType,
            @Body JsonObject body

    );

    @POST(Urls.GET_RESTAURANTS)
    Call<RestaurantResponse> getRest(
            @Query("langu") String lang
    );

    @POST(Urls.GET_RESTAURANTS_DETAILS)
    Call<RestDetailsResponse> getRestDetails(
            @Query("restId") String id,
            @Query("langu") String lang

    );


}

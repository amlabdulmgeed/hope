package com.app.hope.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.app.hope.Network.RetroWeb;
import com.app.hope.Network.ServiceApi;
import com.app.hope.R;
import com.app.hope.UI.Adapters.OrderDetailsAdapter;
import com.app.hope.UI.Adapters.RestDetailsAdapter;
import com.app.hope.UI.Adapters.RestaurantAdapter;
import com.app.hope.base.ParentActivity;
import com.app.hope.models.OrderDetailsModel;
import com.app.hope.models.RestDetailsModel;
import com.app.hope.models.RestDetailsResponse;
import com.app.hope.models.RestaurantModel;
import com.app.hope.models.RestaurantResponse;
import com.app.hope.utils.CommonUtil;

import java.util.ArrayList;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestDetailsActivity extends ParentActivity {

    @BindView(R.id.rv_recycle)
    RecyclerView recyclerView;
    @BindView(R.id.tv_rest_name)
    TextView tvRestName;
    @BindView(R.id.tv_total)
    TextView tvTotal;
    OrderDetailsAdapter adapter;
    ArrayList<OrderDetailsModel> list;


    public static void startActivity(AppCompatActivity mAppCompatActivity, String id, String restName) {
        Intent mIntent = new Intent(mAppCompatActivity, RestDetailsActivity.class);
        mIntent.putExtra("id", id);
        mIntent.putExtra("restName", restName);

        mAppCompatActivity.startActivity(mIntent);
    }

    @Override
    protected void initializeComponents() {
        tvTotal.setVisibility(View.GONE);

        buildRecyclerView();
        tvRestName.setText(getIntent().getExtras().getString("restName"));
        getRestaurantDetails(getIntent().getExtras().getString("id"));



    }

    private void getRestaurantDetails(final String id) {
        RetroWeb.getClients().create(ServiceApi.class).getRestDetails(id, "ar").
                enqueue(new Callback<RestDetailsResponse>() {
                    @Override
                    public void onResponse(Call<RestDetailsResponse> call, Response<RestDetailsResponse> response) {
                        if (response.code() == 200) {
                            for (int i = 0; i < response.body().getX().size(); i++) {
                                if (response.body().getX().get(i).getOrder_user().equals("ali")) {
                                    adapter = new OrderDetailsAdapter(mContext, response.body().getX().get(i).getOrder_details());
                                    recyclerView.setAdapter(adapter);
                                    runLayoutAnimation(recyclerView,R.anim.layout_animation_slide_right);
                                    recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mContext, R.anim.layout_animation_slide_right));
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RestDetailsResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                    }
                });
    }

    private void buildRecyclerView() {


        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new OrderDetailsAdapter(mContext, list);
        recyclerView.setHasFixedSize(true);


    }
   public void openCart(View view){
        ShoppingCartActivity.startActivity((AppCompatActivity)mContext);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_rest_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
}

package com.app.hope.models;

import java.io.Serializable;
import java.util.ArrayList;



public class BaseResponse implements Serializable {

    private String status;
    private String sub_message ;
    private String message;


    public void setMessage(String message) {
        this.message = message;
    }

    private ArrayList<String> errors;

    public ArrayList<String> getErrors() {
        return errors;
    }


    public String getMessage() {
        return message;
    }


    public boolean isSuccessfull() {
        if (status.equals("ok")) {
            return true;
        } else {
            return false;
        }
    }

    public String getSub_message() {
        return sub_message;
    }

    public void setSub_message(String sub_message) {
        this.sub_message = sub_message;
    }
}

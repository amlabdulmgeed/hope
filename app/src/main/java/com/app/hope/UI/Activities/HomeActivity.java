package com.app.hope.UI.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.app.hope.Network.RetroWeb;
import com.app.hope.Network.ServiceApi;
import com.app.hope.R;
import com.app.hope.UI.Adapters.RestaurantAdapter;
import com.app.hope.base.ParentActivity;
import com.app.hope.models.RestaurantModel;
import com.app.hope.models.RestaurantResponse;
import com.app.hope.utils.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends ParentActivity {

    @BindView(R.id.rv_recycle)
    RecyclerView recyclerView;
    RestaurantAdapter adapter;
    @BindView(R.id.tv_total)
    TextView tvTotal;
    ArrayList<RestaurantModel> list;



    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, HomeActivity.class);

        mAppCompatActivity.startActivity(mIntent);

    }

    @Override
    protected void initializeComponents() {
        tvTotal.setVisibility(View.GONE);
        buildRecyclerView();
        getRestaurant();



    }
    private void getRestaurant() {
        RetroWeb.getClients().create(ServiceApi.class).getRest("ar").
                enqueue(new Callback<RestaurantResponse>() {
                    @Override
                    public void onResponse(Call<RestaurantResponse> call, Response<RestaurantResponse> response) {
                        if (response.code() == 200) {
                            adapter = new RestaurantAdapter(mContext, response.body().getX());
                            recyclerView.setAdapter(adapter);
                            runLayoutAnimation(recyclerView,R.anim.layout_animation_slide_right);
                            recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mContext, R.anim.layout_animation_slide_right));
                        }
                    }

                    @Override
                    public void onFailure(Call<RestaurantResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                    }
                });
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void buildRecyclerView() {


        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new RestaurantAdapter(mContext, list);
        recyclerView.setHasFixedSize(true);


    }
    protected void runLayoutAnimation(final RecyclerView recyclerView , int animation ) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context,animation);
        recyclerView.clearAnimation();
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_home;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }
}

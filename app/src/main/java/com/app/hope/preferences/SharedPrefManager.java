package com.app.hope.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.app.hope.models.OrderDetailsModel;
import com.google.gson.Gson;
import com.app.hope.App.Constant;
import com.app.hope.models.UserModel;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;



public class SharedPrefManager {

    Context mContext;

    SharedPreferences mSharedPreferences;

    SharedPreferences.Editor mEditor;

    boolean isGuest ;


    public ArrayList<OrderDetailsModel> getMyCart() {
        ArrayList<OrderDetailsModel> cartModels;
        cartModels = (new Gson()).fromJson(mSharedPreferences.getString(Constant.SharedPrefKey.MY_CART, ""),
                new TypeToken<ArrayList<OrderDetailsModel>>() {
                }.getType());
        return cartModels;
    }

    public void addToMyCart(OrderDetailsModel cartModel) {
        ArrayList<OrderDetailsModel> cartModels = new ArrayList<>();
        if (mSharedPreferences.contains(Constant.SharedPrefKey.MY_CART)) {
            cartModels = (new Gson()).fromJson(mSharedPreferences.getString(Constant.SharedPrefKey.MY_CART, ""),
                    new TypeToken<ArrayList<OrderDetailsModel>>() {
                    }.getType());
        }
        cartModels.add(cartModel);
        mEditor.putString(Constant.SharedPrefKey.MY_CART, (new Gson()).toJson(cartModels));
        mEditor.commit();
    }


    public boolean isGuest() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.GUEST_STATUS, false);
        return value;
    }

    public void setGuest(boolean guest) {
        mEditor.putBoolean(Constant.SharedPrefKey.GUEST_STATUS, guest);
        mEditor.commit();
    }

    public SharedPrefManager(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(Constant.SharedPrefKey.SHARED_PREF_NAME, mContext.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public Boolean getLoginStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.LOGIN_STATUS, false);
        return value;
    }

    public void setLoginStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.LOGIN_STATUS, status);
        mEditor.commit();
    }


    public void setUserData(UserModel userModel) {
        mEditor.putString(Constant.SharedPrefKey.USER, new Gson().toJson(userModel));
        mEditor.apply();
    }

    public UserModel getUserData() {
        Gson gson = new Gson();
        return gson.fromJson(mSharedPreferences.getString(Constant.SharedPrefKey.USER, null), UserModel.class);
    }


    public void Logout() {
        mEditor.clear();
        mEditor.apply();
    }
}

package com.app.hope.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;

import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.app.hope.R;



public final class DialogUtil {

    private DialogUtil() {
    }

    public static ProgressDialog showProgressDialog(Context context, String message, boolean cancelable) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        dialog.setCancelable(cancelable);
        dialog.show();
        return dialog;
    }


    public static AlertDialog showAlertDialog(Context context, String message,
                                              DialogInterface.OnClickListener negativeClickListener) {
        // create the dialog builder & set message
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(message);
        // check negative click listener
        if (negativeClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setNegativeButton(context.getString(R.string.yes), negativeClickListener);


        } else {
            // null
            // add new click listener to dismiss the dialog
            dialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        // create and show the dialog

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
        return dialog;
    }



    public static AlertDialog showAlertDialognotCancelable(Context context, String message,
                                                           DialogInterface.OnClickListener negativeClickListener) {
        // create the dialog builder & set message
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(message);
        // check negative click listener
        if (negativeClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setNegativeButton(context.getString(R.string.ok), negativeClickListener);
        } else {
            // null
            // add new click listener to dismiss the dialog
            dialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        // create and show the dialog

        AlertDialog dialog = dialogBuilder.create();
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }


    public static AlertDialog showconfirm(Context context, int tittle, String message, boolean cancble,
                                          DialogInterface.OnClickListener negativeClickListener, DialogInterface.OnClickListener positiveClickListener) {
        // create the dialog builder & set message
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(tittle);

        dialogBuilder.setMessage(message);

        // check negative click listener
        if (negativeClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setNegativeButton(context.getString(R.string.cancel), negativeClickListener);
        }
        if (positiveClickListener != null) {
            // not null
            // add negative click listener
            dialogBuilder.setPositiveButton(context.getString(R.string.dialog_continue), positiveClickListener);
        }
        // create and show the dialog
        dialogBuilder.setCancelable(cancble);
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }



    public static FancyAlertDialog showFancyDialog (Context mContext , Activity mActivity , String title , String bgColor , String message
            , String negBtnText , String negBtnBg , String posBtnText , String posBtnBg , boolean cancelable , int icon
            , FancyAlertDialogListener posListner , FancyAlertDialogListener negListner )
    {

        FancyAlertDialog dialog = new FancyAlertDialog.Builder(mActivity)
                .setTitle(title)
                .setBackgroundColor(Color.parseColor("#34424D"))  //Don't pass R.color.colorvalue
                .setMessage(message)
                .setNegativeBtnText(negBtnText)
                .setNegativeBtnBackground(Color.parseColor(negBtnBg))  //Don't pass R.color.colorvalue
                .setPositiveBtnText(posBtnText)
                .setPositiveBtnBackground(Color.parseColor("#34424D"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.POP)
                .isCancellable(true)
                .setIcon(icon, Icon.Visible)
                .OnPositiveClicked(posListner)
                .OnNegativeClicked(negListner)
                .build();



        return dialog ;
    }




}

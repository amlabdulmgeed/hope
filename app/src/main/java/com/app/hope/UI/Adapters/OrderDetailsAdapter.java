package com.app.hope.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.hope.R;
import com.app.hope.models.OrderDetailsModel;
import com.app.hope.models.RestDetailsModel;
import com.app.hope.preferences.SharedPrefManager;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.ViewHolder> {

    ArrayList<OrderDetailsModel> arrayList;
    LayoutInflater layoutInflater;
    ItemClickListener clickListener;
    Context context;
    private Animation animationUp;
    private Animation animationDown;
    SharedPrefManager mSharedPrefManager;

    public OrderDetailsAdapter(Context context, ArrayList<OrderDetailsModel> arrayList) {
        this.arrayList = arrayList;
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;


    }

    public OrderDetailsAdapter(ArrayList<OrderDetailsModel> exampleList) {
        arrayList = exampleList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = layoutInflater.inflate(R.layout.item_menue_details, viewGroup, false);
        mSharedPrefManager = new SharedPrefManager(context);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        viewHolder.textView.setText(arrayList.get(i).getProd_name());
        viewHolder.textView1.setText(arrayList.get(i).getProd_quantity());
        viewHolder.textView2.setText(arrayList.get(i).getProd_price() + "$");

        viewHolder.txtContent.setVisibility(View.GONE);

        animationUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        animationDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);

        viewHolder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewHolder.txtContent.isShown()) {
                    viewHolder.txtContent.setVisibility(View.GONE);
                    viewHolder.txtContent.startAnimation(animationUp);
                } else {
                    viewHolder.txtContent.setVisibility(View.VISIBLE);
                    viewHolder.txtContent.startAnimation(animationDown);
                }
            }
        });
        viewHolder.imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSharedPrefManager.addToMyCart(arrayList.get(i));
                Toast.makeText(context,"Added to cart!",Toast.LENGTH_SHORT).show();
            }
        });

        Glide.with(context).load(arrayList.get(i).getProd_image()).asBitmap().placeholder(R.drawable.ic_image_add_button).into(viewHolder.imageView);


    }

    @Override
    public int getItemCount() {
         return arrayList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView, imageView2;
        TextView textView, textView1, textView2, txtContent;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView1 = (TextView) itemView.findViewById(R.id.tv_quantity);
            textView2 = (TextView) itemView.findViewById(R.id.tv_price);

            imageView = (ImageView) itemView.findViewById(R.id.iv_item);

            textView = (TextView) itemView.findViewById(R.id.tv_ctg_name);

            txtContent = (TextView) itemView.findViewById(R.id.title_text);
            imageView2 = (ImageView) itemView.findViewById(R.id.iv_add);


        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public interface ItemClickListener {
        void onClick(View view, int possition);


    }

    public void filterList(ArrayList<OrderDetailsModel> filteredList) {
        arrayList = filteredList;
        notifyDataSetChanged();
    }

}
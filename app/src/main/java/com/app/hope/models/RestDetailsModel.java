package com.app.hope.models;

import java.io.Serializable;
import java.util.ArrayList;

public class RestDetailsModel implements Serializable {
    private String order_id;
    private String order_user;
    private String order_price;
    private String rest_name;
    private String user_location;
    private ArrayList<OrderDetailsModel> order_details;


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_user() {
        return order_user;
    }

    public void setOrder_user(String order_user) {
        this.order_user = order_user;
    }

    public String getOrder_price() {
        return order_price;
    }

    public void setOrder_price(String order_price) {
        this.order_price = order_price;
    }

    public String getRest_name() {
        return rest_name;
    }

    public void setRest_name(String rest_name) {
        this.rest_name = rest_name;
    }

    public String getUser_location() {
        return user_location;
    }

    public void setUser_location(String user_location) {
        this.user_location = user_location;
    }

    public ArrayList<OrderDetailsModel> getOrder_details() {
        return order_details;
    }

    public void setOrder_details(ArrayList<OrderDetailsModel> order_details) {
        this.order_details = order_details;
    }
}

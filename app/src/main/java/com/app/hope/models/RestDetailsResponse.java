package com.app.hope.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RestDetailsResponse extends BaseResponse {
    @SerializedName("return")
    private ArrayList<RestDetailsModel> x;

    public ArrayList<RestDetailsModel> getX() {
        return x;
    }

    public void setX(ArrayList<RestDetailsModel> x) {
        this.x = x;
    }
}
